FROM ubuntu:latest

RUN apt-get update\
&& apt-get install yum-utils alien -y\
&& rpm -Uvh https://mirrors.ripple.com/ripple-repo-el7.rpm\
&& yumdownloader --enablerepo=ripple-stable --releasever=el7 rippled\
&& rpm --import https://mirrors.ripple.com/rpm/RPM-GPG-KEY-ripple-release && rpm -K rippled*.rpm\
&& alien -i --scripts rippled*.rpm && rm rippled*.rpm
RUN openssl req -nodes -x509 -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/server.key -out /etc/ssl/certs/server.crt -subj "/C=DE/ST=NRW/L=Berlin/O=My Inc/OU=DevOps/CN=www.example.com/emailAddress=dev@www.example.com"

WORKDIR /tmp/
COPY /cfg/rippled_standalone.cfg rippled.cfg
COPY entrypoint.sh entrypoint.sh

ENTRYPOINT ["/bin/bash", "entrypoint.sh"]

EXPOSE 51235 5005 6007 6008
