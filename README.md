Docker container with standalone Ripple network for test purposes.

How to run:
````
docker-compose build
docker-compose up
````
Info:
````
ws_public - 6007
wss_public - 6008
rpc_public - 5005
````

If you use nodeJS it may be necessary set `process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"` to connect to WSS.
````
Genesis address = "rHb9CJAWyB4rj91VRWn96DkukG4bwdtyTh"
Genesis private key = "snoPBrXtMeMyMHUVTgbuqAfg1SUTb"
````
You can use it to send XRP to other network clients.