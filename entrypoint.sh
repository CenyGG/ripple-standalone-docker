#!/bin/sh
set -e

cp rippled.cfg /etc/opt/ripple/rippled.cfg && rm rippled.cfg

while true ; do /opt/ripple/bin/rippled ledger_accept & sleep 10; done &
exec /opt/ripple/bin/rippled -a --start --conf=/etc/opt/ripple/rippled.cfg
